//
//  NYTimeAPI.swift
//  NYTimesNews
//
//  Created by Dima on 7/17/19.
//  Copyright © 2019 dmtsolop. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire

class NYTimeAPI {
    
    var articals: Articles?
    
    func apiRequest(theme: String, completion: @escaping (_ articals: Articles)-> Void) {
        let baseURL = String(format:"%@/%@/%@/%@.%@", APIConstants.mostPopularServiceUrl, APIConstants.apiVersion, theme, APIConstants.TimePeriods.oneMonth, APIConstants.requestExtension)
        
        let parameters: Parameters = [
            "api-key": APIConstants.apiKey
        ]
        
        Alamofire.request(baseURL, parameters: parameters).responseJSON() { response in
            guard response.result.error == nil else {
                print("error calling GET ")
                print(response.result.error!)
                return
            }
            
            if let inputJSON = response.data {
                let decoder = JSONDecoder()
                let result = try! decoder.decode(Articles.self, from: inputJSON)
                completion(result)
            } else {
                print("error transmition JSON from Data")
            }
        }
    }
}

struct APIConstants {
    
    static let mostPopularServiceUrl = "https://api.nytimes.com/svc/mostpopular"
    static let apiKey = "qRd7cLQDyJh0yYakyfcY6mdNDqP3a84V"
    
    
    enum Themes : String {
        case mostemailed = "emailed"
        case mostshared = "shared"
        case mostviewed = "viewed"
    }
    
    static let apiVersion = "v2"
    
    struct TimePeriods {
        static let oneMonth = "30"
    }
    
    static let requestExtension = "json"
    
}
