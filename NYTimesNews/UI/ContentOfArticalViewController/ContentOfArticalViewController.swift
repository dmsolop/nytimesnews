//
//  ContentOfArticalViewController.swift
//  NYTimesNews
//
//  Created by Dima on 7/17/19.
//  Copyright © 2019 dmtsolop. All rights reserved.
//

import UIKit
import MagicalRecord

class ContentOfArticalViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    var contentOfArtical: URL?
    
    var isFavorit = false
    var newFavoritArticle: Article!
    var contentOfFavorit: String! {
        didSet {
            print(contentOfFavorit)
        }
    }
    
    //MARK: - Navigation
    static func storyboardInstance() -> ContentOfArticalViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? ContentOfArticalViewController
    }
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let addFavoritBtn = UIBarButtonItem(image: UIImage(named: "addToFavorit"), style: .plain, target: self, action: #selector(addFavoritBtnAction(sender:)))
        self.navigationItem.rightBarButtonItem = addFavoritBtn
        
        switch isFavorit {
        case false:
            webView.loadRequest(URLRequest(url: contentOfArtical!))
        case true:
            webView.loadHTMLString(contentOfFavorit, baseURL: nil)
        }
        
    }
    
    @objc func addFavoritBtnAction(sender: UIBarButtonItem) {
        
        DispatchQueue.main.async {
            if let contentURL = self.contentOfArtical {
                do {
                    let content = try String(contentsOf: contentURL)
                    let title = self.getTitle(text: content)
                    
                    self.newFavoritArticle = Article.createEntity() as? Article
                    self.newFavoritArticle.title = title
                    self.newFavoritArticle.content = content
                    self.newFavoritArticle.isFavorit = true
                    self.saveContext()
                    
                } catch {
                    print("error: ", error)
                }
            }
        }
    }
    
    func saveContext() {
        NSManagedObjectContext.default().saveToPersistentStoreAndWait()
    }
    
    func getTitle(text: String) -> String {
        if let endIndex = text.range(of: "</title>")?.lowerBound, let startIndex = text.range(of: "<title data-rh=\"true\">")?.upperBound {
            return String(text[startIndex..<endIndex])
        }
        return ""
    }
}
