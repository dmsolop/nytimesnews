//
//  ListOfArticalViewController.swift
//  NYTimesNews
//
//  Created by Dima on 7/17/19.
//  Copyright © 2019 dmtsolop. All rights reserved.
//

import UIKit

class ListOfArticleViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var isFavorit: Bool = false
    
    var articles: Articles? {
        didSet {
            isFavorit = false
        }
    }
    var favoritArticles: [Article]? {
        didSet {
            isFavorit = true
        }
    }
    
    //MARK: - Navigation
    static func storyboardInstance() -> ListOfArticleViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? ListOfArticleViewController
    }
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func saveContext() {
        NSManagedObjectContext.default().saveToPersistentStoreAndWait()
    }
}

extension ListOfArticleViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch isFavorit {
        case false:
            return articles?.results.count ?? 0
        case true:
            return favoritArticles?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ListOfArticalTableViewCell.self), for: indexPath)
        
        switch isFavorit {
        case false:
            if let array = self.articles?.results {
                cell.textLabel?.text = array[indexPath.row].title
                return cell
            }
        case true:
            cell.textLabel?.text = favoritArticles?[indexPath.row].title
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contentOfArticalViewController = ContentOfArticalViewController.storyboardInstance()
        switch isFavorit {
        case false:
            contentOfArticalViewController?.contentOfArtical = articles?.results[indexPath.row].url
        case true:
            contentOfArticalViewController?.isFavorit = true
            contentOfArticalViewController?.contentOfFavorit = favoritArticles![indexPath.row].content
        }
        self.navigationController?.pushViewController(contentOfArticalViewController!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if isFavorit {
            favoritArticles?.remove(at: indexPath.row).deleteEntity()
            saveContext()
            
            let indexPaths = [indexPath]
            tableView.deleteRows(at: indexPaths, with: .automatic)
            tableView.reloadData()
        }

    }
    
}
