//
//  StartScreenViewController.swift
//  NYTimesNews
//
//  Created by Dima on 7/17/19.
//  Copyright © 2019 dmtsolop. All rights reserved.
//

import UIKit

class StartScreenViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let backgroungImageView = UIImageView()
    let request = NYTimeAPI()
    
    //MARK: - Navigation
    static func storyboardInstance() -> StartScreenViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? StartScreenViewController
    }
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bgImage = UIImageView();
        bgImage.image = UIImage(named: "BackgroundImage");
        bgImage.contentMode = .scaleToFill
        
        self.collectionView?.backgroundView = bgImage
        self.collectionView.backgroundView?.alpha = 0.25
        self.collectionView.backgroundColor = UIColor.white
    }
    
    //MARK: - APIRequest methods
    func getResponseFromApi(theme: String) {
        request.apiRequest(theme: theme) { articals in
            let result = articals
            DispatchQueue.main.async {
                self.fillListOfArticals(articals: result)
            }
        }
    }
    
    func getFavoritArticles() {
        DispatchQueue.main.async {
            let favoritArt: [Article] = (Article.findAll() as! [Article])
            self.fillListOfArticals(favoritArticles: favoritArt)
        }

    }
    
    func fillListOfArticals(articals: Articles?) {
        
        let listOfArticalViewController = ListOfArticleViewController.storyboardInstance()
        listOfArticalViewController?.articles = articals
        self.navigationController?.pushViewController(listOfArticalViewController!, animated: true)
    }
    
    func fillListOfArticals(favoritArticles: [Article]?) {
        
        let listOfArticalViewController = ListOfArticleViewController.storyboardInstance()
        listOfArticalViewController?.favoritArticles = favoritArticles
        self.navigationController?.pushViewController(listOfArticalViewController!, animated: true)
    }
    
}


extension StartScreenViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constants.numberItemsStartCollectionView
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = String(describing: StartScreenCollectionViewCell.self)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! StartScreenCollectionViewCell
        
        cell.index = indexPath.row
        cell.setName(index: indexPath.row)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            getResponseFromApi(theme: APIConstants.Themes.mostemailed.rawValue)
        case 1:
            getResponseFromApi(theme: APIConstants.Themes.mostshared.rawValue)
        case 2:
            getResponseFromApi(theme: APIConstants.Themes.mostviewed.rawValue)
        case 3:
            getFavoritArticles()
        default: break
        }
    }
    
}
