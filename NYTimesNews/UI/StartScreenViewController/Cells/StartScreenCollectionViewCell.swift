//
//  StartScreenCollectionViewCell.swift
//  NYTimesNews
//
//  Created by Dima on 7/17/19.
//  Copyright © 2019 dmtsolop. All rights reserved.
//

import UIKit

class StartScreenCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var NameOfCell: UILabel!
    
    var index: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 4
        self.backgroundColor = UIColor.lightGray
    }
    
    func setName(index: Int) {
        
        switch index {
        case 0:
            self.NameOfCell.text = APIConstants.Themes.mostemailed.rawValue
        case 1:
            self.NameOfCell.text = APIConstants.Themes.mostshared.rawValue
        case 2:
            self.NameOfCell.text = APIConstants.Themes.mostviewed.rawValue
        case 3:
            self.NameOfCell.text = "favorites"
        default: break
        
        }
    } 
}
