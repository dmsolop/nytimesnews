//
//  NYTimesNews-BridgingHeader.h
//  NYTimesNews
//
//  Created by Dima on 7/23/19.
//  Copyright © 2019 dmtsolop. All rights reserved.
//

#ifndef NYTimesNews_BridgingHeader_h
#define NYTimesNews_BridgingHeader_h

#define MR_SHORTHAND

// &lt; = <  and  $gt; = >
//#import &lt;CoreData+MagicalRecord.h&gt;
#import <Foundation/Foundation.h>

#import <CoreData+MagicalRecord.h>

#endif /* NYTimesNews_BridgingHeader_h */
