//
//  Article.swift
//  NYTimesNews
//
//  Created by Dima on 7/26/19.
//  Copyright © 2019 dmtsolop. All rights reserved.
//

import Foundation
import CoreData

@objc(Article)
class Article: NSManagedObject {
    
    // Attributes
    @NSManaged var title: String
    @NSManaged var content: String
    @NSManaged var isFavorit: Bool
    
    // Relationships
    @NSManaged var images: [ImageOfArticle]
    
}
