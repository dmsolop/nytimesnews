//
//  ImageOfArticle.swift
//  NYTimesNews
//
//  Created by Dima on 7/26/19.
//  Copyright © 2019 dmtsolop. All rights reserved.
//

import Foundation
import CoreData

@objc(ImageOfArticle)
class ImageOfArticle: NSManagedObject {
    
    // Attributes
    @NSManaged var image: Data
    
    // Relationships
    @NSManaged var article: Article
    
}
