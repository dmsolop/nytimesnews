//
//  Articals.swift
//  NYTimesNews
//
//  Created by Dima on 7/17/19.
//  Copyright © 2019 dmtsolop. All rights reserved.
//

import Foundation

struct Articles: Codable {
    struct Artical: Codable {
        let url: URL
        let title: String
    }
    let results: [Artical]
}

