//
//  Constants.swift
//  NYTimesNews
//
//  Created by Dima on 7/17/19.
//  Copyright © 2019 dmtsolop. All rights reserved.
//

import Foundation

enum Constants {
    
    static let numberItemsStartCollectionView = 4
    static let replacingCharsForString = "^^^^^^^^^^^^^^"
    
}
